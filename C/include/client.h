#ifndef _CLIENT_
#define _CLIENT_

#include <gtk/gtk.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <stdbool.h>
#define PORT 2017
#define BUF_SIZE 2000

//gestion client et thread annexes
void* recevoirMessageServeur(void* );
void* initialisation(void*);
void* miseAjourPhase(void*); //charger de changer en temps reel la phase
void* miseAjourTirage(void*);
void* miseAjourScore (void* );
void* miseAjourMessage(void*);

//interface graphique gtk
void fenetre_principale(void);
GtkWidget* dessiner_plateau();
GtkWidget* dessiner_tirage();
GtkWidget* dessiner_timer();
GtkWidget* dessiner_phase();
GtkWidget* dessiner_soumission();
GtkWidget* dessiner_quitter();

void changer_contenueTirage(gchar* img ,int position);
GtkWidget* creer_tirageVide ();


void insert_text(char*);
GtkWidget* create_list();
GtkWidget* create_text(char*);
GtkWidget*  gestion_message();
void fenetre_connexion(void);

/*fonction handler bouton connexion , soumission , quitter*/
void callback_connexion (GtkWidget *widget, gpointer *data);
void callback_soumission (GtkWidget *widget, gpointer *data);
void callback_quitter (GtkWidget *widget, gpointer *data);

//

gchar *lettreTirage[27]={
"../scrabble/lettres/A.png",\
"../scrabble/lettres/B.png",\
"../scrabble/lettres/C.png",\
"../scrabble/lettres/D.png",\
"../scrabble/lettres/E.png",\
"../scrabble/lettres/F.png",\
"../scrabble/lettres/G.png",\
"../scrabble/lettres/H.png",\
"../scrabble/lettres/I.png",\
"../scrabble/lettres/J.png",\
"../scrabble/lettres/K.png",\
"../scrabble/lettres/L.png",\
"../scrabble/lettres/M.png",\
"../scrabble/lettres/N.png",\
"../scrabble/lettres/O.png",\
"../scrabble/lettres/P.png",\
"../scrabble/lettres/Q.png",\
"../scrabble/lettres/R.png",\
"../scrabble/lettres/S.png",\
"../scrabble/lettres/T.png",\
"../scrabble/lettres/U.png",\
"../scrabble/lettres/V.png",\
"../scrabble/lettres/W.png",\
"../scrabble/lettres/X.png",\
"../scrabble/lettres/Y.png",\
"../scrabble/lettres/Z.png",\
"../scrabble/lettres/*.png"};

#endif
