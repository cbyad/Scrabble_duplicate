#include "../include/client.h"
#include "../include/autres.h"

//deja declaré et initialisé dans autres.h
extern gchar* lettreTirage[27];

//variable globale prog
char* TRAITEMENT=NULL; // var global pour requette client
int SCORE=0;
char* TIRAGE =NULL;
char* PHASE=NULL ; // DEB, REC, SOU, RES
char* USER=NULL;
char* LIST_MESSAGES = NULL ;
pthread_mutex_t mutex_traitement, mutex_user,
mutex_tirage ,mutex_phase, mutex_score,mutex_message;

pthread_cond_t condition =PTHREAD_COND_INITIALIZER;
pthread_cond_t condition_tirage =PTHREAD_COND_INITIALIZER;
pthread_cond_t condition_phase = PTHREAD_COND_INITIALIZER;
pthread_cond_t condition_score = PTHREAD_COND_INITIALIZER;
pthread_cond_t condition_message = PTHREAD_COND_INITIALIZER;
//pour actualiser l'ihm en temps reel

GtkWidget *fenetreConnexion;

//variable global ihm
GtkWidget* typePhase;
GtkWidget *fenetre ,*hpaned; // le tout
GtkWidget *vbox_gauche ,*plateau,*tirage ,*timer; //gauche
GtkWidget *vbox_droite , *Scores ,*text ,*quitter,*soumission;//droite
//message ;
GtkWidget* btn;
GtkTextBuffer *buffer;
GtkWidget *forum, *message, *view;

//pour le timer
static float percent =0.0;

static gboolean inc_progress(gpointer data)
{
  percent += 0.01;
  if(percent >= 1.0) return TRUE ;
  //percent = 0.0;
  gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(data), percent);

  char c[3];
  sprintf(c, "%d%", (int)(percent*100));
  gtk_progress_bar_set_text(GTK_PROGRESS_BAR(data), c);
}


//---------MAIN PRINCIPAL CLIENT ET IHM----------------

int main (int argc, char **argv)
{
  int ret  ;
  pthread_t thread_ihm ;
  pthread_t thread_phase ;

  pthread_t thread_tirage ;
  pthread_t thread_message ;
  //pthread_t thread_score ;

  pthread_mutex_init(&mutex_user,NULL);
  pthread_mutex_init(&mutex_traitement,NULL);
  pthread_mutex_init(&mutex_tirage,NULL);
  pthread_mutex_init(&mutex_phase,NULL);
  pthread_mutex_init(&mutex_score,NULL);
  pthread_mutex_init(&mutex_message,NULL);

  gtk_init (&argc, &argv);

  fenetre_connexion();

  //lance le client
  ret=pthread_create(&thread_ihm, NULL,initialisation,NULL);

  if(ret<0) {
    printf("ERREUR: code de retour pthread_create() : %d\n", ret);
    exit(1);
  }

  pthread_create(&thread_phase,NULL,miseAjourPhase,NULL);
  pthread_create(&thread_tirage,NULL,miseAjourTirage,NULL);
  pthread_create(&thread_message,NULL,miseAjourMessage,NULL);
  //pthread_create(&thread_score, NULL,miseAjourScore,NULL);

  fenetre_principale();
  gtk_main();

  pthread_join(thread_ihm,NULL);
  pthread_join(thread_phase,NULL);
  pthread_join(thread_tirage,NULL);
  pthread_join(thread_message,NULL);
  //pthread_join(thread_score,NULL);

  return 0;
}
//------------------__FIN_PRINCIPAL_________________________________//


/*----------------------la fenetre  du jeu--------------------*/
void fenetre_principale()
{

  fenetre = gtk_window_new	(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(fenetre),"Scrabble-duplicate");
  gtk_window_set_default_size	(GTK_WINDOW(fenetre), 900, 400);
  g_signal_connect(G_OBJECT(fenetre),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  gtk_window_set_position(GTK_WINDOW(fenetre),GTK_WIN_POS_CENTER);
  //partie gauche de l'ecran
  plateau = dessiner_plateau();
  tirage = dessiner_tirage();
  timer= dessiner_timer();
  vbox_gauche = gtk_vbox_new(FALSE,0);
  gtk_box_pack_start (GTK_BOX (vbox_gauche), plateau, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox_gauche), tirage, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox_gauche), timer, TRUE, TRUE,0);

  //partie droite de l'ecran
  Scores = create_list();
  text = gestion_message();
  typePhase= dessiner_phase();

  soumission= dessiner_soumission();
  quitter = dessiner_quitter();
  vbox_droite= gtk_vbox_new(FALSE,0);
  gtk_box_pack_start (GTK_BOX (vbox_droite), Scores, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox_droite), text, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox_droite), typePhase, TRUE, TRUE,0);
  gtk_box_pack_start (GTK_BOX (vbox_droite), soumission, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox_droite), quitter, TRUE, TRUE, 0);

  // gauche et droite ensemble :-) haha
  hpaned = gtk_hpaned_new ();
  gtk_container_add (GTK_CONTAINER (fenetre), hpaned);
  gtk_paned_add1(GTK_PANED (hpaned), vbox_gauche);
  gtk_paned_add2(GTK_PANED (hpaned), vbox_droite);

  g_signal_connect(G_OBJECT(quitter),"clicked",G_CALLBACK(callback_quitter),NULL);
  g_signal_connect(G_OBJECT(soumission),"clicked",G_CALLBACK(callback_soumission),NULL);

  gtk_widget_show_all(fenetre);
}

void fenetre_connexion(){
  GtkWidget  *label ,*entry,*button,*hbox;

  gint row , col ;
  fenetreConnexion = gtk_window_new	(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(fenetreConnexion),"Scrabble-duplicate/connexion");
  gtk_window_set_default_size	(GTK_WINDOW(fenetreConnexion), 400,300);
  gtk_window_set_position(GTK_WINDOW(fenetreConnexion), GTK_WIN_POS_CENTER);
  g_signal_connect(G_OBJECT(fenetreConnexion),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  label= gtk_label_new ("Votre pseudo joueur :");
  button =gtk_button_new_with_mnemonic ("Connexion...");
  gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NORMAL);

  entry = gtk_entry_new();
  gtk_entry_set_text (GTK_ENTRY (entry),"");
  g_signal_connect(G_OBJECT(button),"clicked",G_CALLBACK(callback_connexion),(void*)entry);

  hbox = gtk_hbox_new (TRUE, 3);
  gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE,0);

  gtk_container_add (GTK_CONTAINER (fenetreConnexion), hbox);
  gtk_widget_show_all (fenetreConnexion);

}

//-------------------fonction de traitement click button------------------
void callback_connexion (GtkWidget *widget, gpointer *data)
{
  const char* user=(char*)gtk_entry_get_text(GTK_ENTRY(data));

  if(strcmp(user,"")!=0){
    char* connexion ="CONNEXION/";
    int taille =(int)gtk_entry_get_text_length(GTK_ENTRY(data)) ;
    char* tmp= malloc((taille+strlen(connexion))* sizeof(char));

    strcpy(tmp,connexion);
    char* tmp2 = malloc(sizeof(char)*(strlen(tmp+1)));
    tmp2 = strcat(tmp,user);

    //traitement -->variable partagée!!!
    pthread_mutex_lock(&mutex_traitement);

    TRAITEMENT= malloc((taille+strlen(connexion+1))* sizeof(char));
    TRAITEMENT=strcat(tmp2,"/\n");

    pthread_mutex_lock(&mutex_user);

    USER=malloc(taille*sizeof(char));
    strcpy(USER,user);
    pthread_mutex_unlock(&mutex_user);

    fputs(TRAITEMENT,stdout);

    pthread_mutex_unlock(&mutex_traitement);
    pthread_cond_signal(&condition);

    g_timeout_add(1200, inc_progress,timer); //2min
    //gtk_widget_destroy(fenetreConnexion); // fermer la fenetre
  }

}

void callback_quitter (GtkWidget *widget, gpointer *data){

  pthread_mutex_lock(&mutex_user);
  if (USER!=NULL) {

    char* sort="SORT/";
    char* tmp = malloc((strlen(sort)+strlen(USER))*sizeof(char)*2 );
    tmp=strcat(tmp,sort);
    tmp=strcat(tmp,USER);

    pthread_mutex_unlock(&mutex_user);

    pthread_mutex_lock(&mutex_traitement);

    free(TRAITEMENT);
    TRAITEMENT=NULL;
    TRAITEMENT= malloc((strlen(sort)+strlen(USER))*sizeof(char)*2 );
    strcpy(TRAITEMENT,tmp);
    TRAITEMENT=strcat(TRAITEMENT,"/\n");
    fputs(TRAITEMENT,stdout);
    pthread_mutex_unlock(&mutex_traitement);
    pthread_cond_signal(&condition);
    gtk_widget_destroy(fenetreConnexion);
    gtk_widget_destroy(fenetre);
  }
  else return ;
}

void callback_soumission (GtkWidget *widget, gpointer *data){

}

/*plateau pour le scrabble*/
GtkWidget* dessiner_plateau(){

  gint row ,col ;
  const int NB_CASES= 15 ;
  gchar *img[2]={"../scrabble/red.png", "../scrabble/bleu.png"};

  GtkWidget* table , *image ,*btn;
  //*label  ;
  table = gtk_table_new(NB_CASES,NB_CASES,FALSE);
  gtk_table_set_row_spacings(GTK_TABLE(table),1);

  for (row=0 ; row<NB_CASES ;row++) {
    for (col=0 ; col<NB_CASES ;col++ ) {

      if( (col==0 && (row ==0 || row ==7 || row==14) ) || (
        col ==14 && (row ==0 || row ==7 || row ==14))) {
          image = gtk_image_new_from_file(img[0]);
          gtk_table_attach_defaults(GTK_TABLE (table),image, row,row+1, col,col+1);
        }
        else{
          btn = gtk_button_new();
          gtk_button_set_relief(GTK_BUTTON(btn), GTK_RELIEF_NONE);
          image = gtk_image_new_from_file(img[1]);
          gtk_container_add (GTK_CONTAINER (btn), image);

          //label = gtk_label_new (g_strdup_printf("[%d-%d]", row, col));
          gtk_table_attach_defaults(GTK_TABLE (table),btn, row,row+1, col,col+1);
        }
      }
    }
    return table ;
  }


  GtkWidget* dessiner_tirage(){

    gint row ,col ;
    const int NB_COL=7;
    const int NB_LIGNE=1;
    gchar *img[1]={"../scrabble/lettres/A.png"}; // par defaut

    GtkWidget  *image ,*btn;
    //*label  ;
    tirage = creer_tirageVide(); // pour differencier de tirage global

    for (row=0 ; row<NB_LIGNE ;row++) {
      for (col=0 ; col<NB_COL ;col++ ) {

        btn = gtk_button_new();
        gtk_button_set_relief(GTK_BUTTON(btn), GTK_RELIEF_NONE);
        image = gtk_image_new_from_file(img[0]);
        gtk_container_add (GTK_CONTAINER (btn), image);

        //GtkWidget* label = gtk_label_new (g_strdup_printf("[%d-%d]", row, col));
        gtk_table_attach(GTK_TABLE (tirage),btn, col,col+1, row,row+1,0,0,0,0);
      }
    }
    return tirage ;
  }

  GtkWidget* creer_tirageVide (){

    const int NB_COL=7;
    const int NB_LIGNE=1;
    GtkWidget* _tirage ;

    _tirage = gtk_table_new(NB_LIGNE,NB_COL,TRUE);
    gtk_table_set_row_spacings(GTK_TABLE(_tirage),1);
    return _tirage;
  }

  void changer_contenueTirage(gchar* img ,int position){

    GtkWidget *image ;
    if(position<0 || position>=7) exit(1) ;


    btn = gtk_button_new();
    gtk_button_set_relief(GTK_BUTTON(btn), GTK_RELIEF_NONE);
    image = gtk_image_new_from_file(img);
  //  printf("nom de l'image a ajouter %s\n",img);
    gtk_container_add (GTK_CONTAINER (btn), image);

    gtk_table_attach(GTK_TABLE (tirage),btn, position,position+1, 0,0+1,0,0,0,0);
  }

  GtkWidget* dessiner_timer(){

    GtkWidget* timer =gtk_progress_bar_new();
    gtk_progress_bar_set_orientation(GTK_PROGRESS_BAR(timer),
    GTK_PROGRESS_LEFT_TO_RIGHT);

    return timer ;
  }


  GtkWidget* dessiner_phase(){
    return gtk_label_new("Type phase :");

  }

  GtkWidget* dessiner_soumission(){
    GtkWidget* soumission = gtk_button_new_with_mnemonic("Soumission");
    gtk_button_set_relief(GTK_BUTTON(soumission), GTK_RELIEF_NORMAL);
    return soumission;
  }

  GtkWidget* dessiner_quitter(){

    GtkWidget* quitter = gtk_button_new_with_mnemonic("Quitter partie");
    gtk_button_set_relief(GTK_BUTTON(quitter), GTK_RELIEF_NORMAL);
    return quitter;
  }


  /* Creer une liste de "messages" */
  GtkWidget* create_list( )
  {
    GtkWidget *scrolled_window;
    GtkWidget *tree_view;
    GtkListStore *model;
    GtkTreeIter iter;
    GtkCellRenderer *cell;
    GtkTreeViewColumn *column;

    int i;

    /* Une fenetre scrollable  */
    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
    GTK_POLICY_AUTOMATIC,
    GTK_POLICY_AUTOMATIC);

    model = gtk_list_store_new (1, G_TYPE_STRING);
    tree_view = gtk_tree_view_new ();
    gtk_container_add (GTK_CONTAINER (scrolled_window), tree_view);
    gtk_tree_view_set_model (GTK_TREE_VIEW (tree_view), GTK_TREE_MODEL (model));
    gtk_widget_show (tree_view);

    /* ajouter messages a la fenetre  */
    for (i = 0; i < 20; i++) {
      gchar *msg = g_strdup_printf ("User #%d", i);
      gtk_list_store_append (GTK_LIST_STORE (model), &iter);
      gtk_list_store_set (GTK_LIST_STORE (model),
      &iter,
      0, msg,
      -1);
      g_free (msg);
    }

    cell = gtk_cell_renderer_text_new ();

    column = gtk_tree_view_column_new_with_attributes ("Scores",
    cell,
    "text", 0,
    NULL);

    gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view),
    GTK_TREE_VIEW_COLUMN (column));

    return scrolled_window;
  }

  /*inserer du texte */
  void insert_text(char* msg)
  {
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_offset (buffer, &iter, 0);
    gtk_text_buffer_insert (buffer, &iter, msg, -1);
  }

  /* Creer une zone de text scrollabe qui affiche un "message" */
  GtkWidget *create_text(char* msg )
  {
    GtkWidget *scrolled_window;

    view = gtk_text_view_new ();
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));

    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
    GTK_POLICY_AUTOMATIC,
    GTK_POLICY_AUTOMATIC);

    gtk_container_add (GTK_CONTAINER (scrolled_window), view);
    insert_text (msg);

    gtk_widget_show_all (scrolled_window);
    return scrolled_window;
  }

  GtkWidget*  gestion_message()
  {

    GtkWidget *notebook;
    GtkWidget *label;

    notebook = gtk_notebook_new ();
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (notebook), GTK_POS_TOP);

    forum= create_text("ici forum\n");
    label = gtk_label_new ("Chat");
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook),forum,label);

    message= create_text("ici prive\n");
    label=gtk_label_new("message");
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook),message,label);

    return notebook ;
  }


  //----------------------code gestion client---------------------
  void * recevoirMessageServeur(void * socket) {
    int sockfd, ret;
    int taille=0 ;
    int i;
    char buffer[BUF_SIZE];
    sockfd = (int)socket;
    memset(buffer, 0, BUF_SIZE);

    while(true){
      ret = recvfrom(sockfd, buffer, BUF_SIZE, 0, NULL, NULL);
      if (ret < 0) {
        printf("Erreur de reception de donnée !\n");
      } else {
        printf("S->C : ");
        fputs(buffer, stdout);
        //traiter reponse serveur

        char* reponse = malloc(BUF_SIZE*sizeof(char));
        strcpy(reponse,buffer);
        char** ligne = split(reponse,"/",&taille);

        // -------------interpretation des reponse serveur----------
        if(strcmp("BIENVENUE",ligne[0])==0 ){

          if(strcmp("videx",ligne[2])!=0){ // si pas videx
            pthread_mutex_lock(&mutex_tirage);
            if(TIRAGE!=NULL){
              free(TIRAGE);TIRAGE=NULL;
            }
            TIRAGE= malloc(strlen(ligne[2])*sizeof(char));
            strcpy(TIRAGE,ligne[2]);
            pthread_mutex_unlock(&mutex_tirage);
            pthread_cond_signal(&condition_tirage);
          }
          pthread_mutex_lock(&mutex_phase);
          if(PHASE!=NULL){ //eviter gachis memoire
            free(PHASE); PHASE= NULL ;
          }

          PHASE = malloc( strlen(ligne[4])*sizeof(char) );
          strcpy(PHASE,ligne[4]);
          pthread_mutex_unlock(&mutex_phase);
          pthread_cond_signal(&condition_phase);

        }

        if(strcmp("TOUR",ligne[0])==0){
          //pthread_mutex_lock(&mutex_tirage);
          if(TIRAGE!=NULL ){ // eviter gachis memoire
            free(TIRAGE);
            TIRAGE=NULL ;
          }
          TIRAGE= malloc( strlen(ligne[2])*sizeof(char) );
          strcpy(TIRAGE,ligne[2]);

          pthread_mutex_unlock(&mutex_tirage);
          pthread_cond_signal(&condition_tirage);

        }

        if (strcmp("RECEPTION",ligne[0])==0){
          // message
        }

        if (strcmp("PRECEPTION",ligne[0])==0){
          pthread_mutex_lock(&mutex_message);
          int a = strlen(ligne[1])+1; // ; message
          int b = strlen(ligne[2])+1 ; // \n  destinataire

          LIST_MESSAGES= realloc(LIST_MESSAGES,(a+b)*sizeof(char));
          LIST_MESSAGES=strcat(LIST_MESSAGES,ligne[2]);
          LIST_MESSAGES=strcat(LIST_MESSAGES,":");
          LIST_MESSAGES=strcat(LIST_MESSAGES,ligne[1]);
          LIST_MESSAGES=strcat(LIST_MESSAGES,"\n");

          pthread_mutex_unlock(&mutex_message);
          pthread_cond_signal(&condition_message);
        }
      }
    }
  }

  void* initialisation(void * arg){
    int i =0;
    struct sockaddr_in serv_addr; // addresse serveur
    int clientSocket, ret;
    char buffer[BUF_SIZE];

    socklen_t slen=sizeof(serv_addr);
    pthread_t recepteurMessage;
    /*creation de la socket */
    clientSocket=socket(AF_INET, SOCK_STREAM, 0);
    if(clientSocket<1) {
      printf("Erreur de creation de la socket!\n");
      exit(1);
    }
    printf("Socket cree avec succes...\n");

    /*configuration du serveur */
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr =htonl(INADDR_ANY);
    serv_addr.sin_port = htons(PORT);


    /*connecter la socket au serveur */
    ret = connect(clientSocket, (struct sockaddr *) &serv_addr, slen);
    if (ret < 0) {
      printf("Erreur de connection au serveur !\n");
      exit(1);
    }
    printf("Connecté au serveur...\n");
    // lancer ihm

    memset(buffer, 0, BUF_SIZE);

    //creation d'une nouvelle thread pour la recpetion des messages du serveur
    ret = pthread_create(&recepteurMessage, NULL,recevoirMessageServeur,(void*)clientSocket);
    if (ret) {
      printf("ERREUR: code de retour pthread_create() : %d\n", ret);
      exit(1);
    }

    pthread_mutex_lock(&mutex_traitement);
    pthread_cond_wait(&condition,&mutex_traitement);
    while (TRAITEMENT!=NULL) {

      for(i=0; i<strlen(TRAITEMENT);i++){
        buffer[i]=TRAITEMENT[i];
      }
      //printf("etat requette dans le thread ---> %s\n", TRAITEMENT);
      ret = sendto(clientSocket, buffer, BUF_SIZE, 0, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
      //traitement=NULL;
      pthread_mutex_unlock(&mutex_traitement);

      if (ret < 0) {
        printf("Erreur d'envoie de donnée !\n\t-%s", buffer);
      }
    }
    close(clientSocket);
    pthread_exit(NULL);

  }

  //----------threads annexes pour la mise a jour des composants graphiques---------------

  void* miseAjourPhase(void* arg){

    while (true) {
      pthread_mutex_lock(&mutex_phase);
      pthread_cond_wait (&condition_phase,&mutex_phase);

      char* p= "Type Phase : ";
      char* str = malloc (sizeof(char)*20);
      strcpy(str,p);
      str= strcat(str,PHASE);
      gtk_label_set_text (GTK_LABEL(typePhase),str);
      free(str);
      pthread_mutex_unlock(&mutex_phase);
    }

  }

  void* miseAjourTirage(void* arg){

    while (true) {
      pthread_mutex_lock(&mutex_tirage);
      pthread_cond_wait (&condition_tirage,&mutex_tirage);
      int i =0 ;

      for (i=0 ; i<strlen(TIRAGE);i++){

        if(get_char(TIRAGE,i)=='*'){
          changer_contenueTirage((gchar*)lettreTirage[26],i);
        }
        else{
          changer_contenueTirage(lettreTirage[get_char(TIRAGE,i)-'A'],i);

        }
      }
      pthread_mutex_unlock(&mutex_tirage);
    }

  }

  void* miseAjourMessage(void* arg) {

    while (true){

      pthread_mutex_lock(&mutex_message);
      pthread_cond_wait (&condition_message,&mutex_message);

      message=create_text(LIST_MESSAGES);
      insert_text(LIST_MESSAGES);

      pthread_mutex_unlock(&mutex_message);
    }

  }
