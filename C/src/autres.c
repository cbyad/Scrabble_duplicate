
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../include/autres.h"

char** split(char* chaine, char* delimiter, int * taille_resultat) {
  char** resultat;
  char* token;
  int i = 0;
  char* chaine_safe = strdup(chaine);

  token = strtok(chaine_safe, delimiter);
  //rien a faire
  if (token == NULL)
  return NULL;

  resultat = (char**) malloc(sizeof(char*));
  resultat[i] = (char*) malloc(strlen(token) * sizeof(char));
  strcpy(resultat[i], strdup(token));

  while ((token = strtok(NULL, delimiter)) != NULL) {
    i++;
    resultat = (char**) realloc(resultat, (i+1) * sizeof(char*));
    resultat[i] = (char*)malloc((strlen(token)+1) * sizeof(char));
    strcpy(resultat[i], strdup(token));
  }
  *taille_resultat = i+1;
  free(token);
  return resultat;
}

// retourne la 1ere occurence trouvé
char get_char(char* chaine, int index){

  size_t taille = strlen(chaine);
  int i= 0;

  if(index>=0 && index<taille){
    for(i=0 ;i<taille;i++){
      if(i==index)
      return chaine[i];
    }
  }
  else return '!'; // on a rien trouve ou index incoherent

}
