package com.pc2r.serveur;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
public class Serveur extends Thread {
	protected static final int PORT = 2017;
	protected ServerSocket ecoute;
	protected Scrabble scrab;
	protected Messager messager;
	protected int limiteJoueur;
	private ExecutorService pool;
	private ArrayList<SessionUtilisateur> sessionUtilisateurs;
	protected OperationsSurScrabble opScrab;

	//Serveur permettant de lancer le Scrabble, le messager et le pool de thread(Session utilisateur
	public Serveur(int limiteJoueurC) {
		try { 
			pool = Executors.newFixedThreadPool(limiteJoueurC);
			ecoute = new ServerSocket(PORT);
			messager = new Messager();
			limiteJoueur = limiteJoueurC;
			scrab = new Scrabble(messager);
			opScrab = new OperationsSurScrabble(scrab);
			sessionUtilisateurs = new ArrayList<SessionUtilisateur>();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
		System.out.println("En écoute sur le port : "+ PORT);
		this.start();
		scrab.start();
		
	}
	//POOL de thread pour limiter le nombre connexion
	public void run(){
		try{
			Socket client;
			while(true){
				System.out.println("en attente");
				client = ecoute.accept();
				if (sessionUtilisateurs.size() >= limiteJoueur) {
					refuserClient(client);
				} else {
					SessionUtilisateur client1 = new SessionUtilisateur(client, scrab, opScrab);
					sessionUtilisateurs.add(client1);
					pool.execute(client1);
				}

			}
		}
		catch(IOException e){
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	private void refuserClient(Socket socket) {
		try {
			PrintWriter out = new PrintWriter(socket.getOutputStream());

			out.println("REFUS/");
			out.flush();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner( System.in );
		System.out.println("entrer le nombre d'utilisateur maximum");
		int x = sc.nextInt();
		new Serveur(x);
		sc.close();
	}
}
