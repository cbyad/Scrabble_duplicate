package com.pc2r.serveur;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Classe permettant de d'envoyer des messages à tous les utilisateurs
public class Messager extends Thread{
	List< SessionUtilisateur> listeUtilisateurs ;
	List< String> listeMessages ;
	Scrabble monScrabble ;
	public Messager() {
		listeUtilisateurs = new ArrayList<>();
		listeMessages = new ArrayList<>();
	}
	
	
	public void run (){
		
		while(true){
			try {
				synchronized (this) {
					wait();
				}
				while(!listeMessages.isEmpty()){
					envoiMessage(listeMessages.get(0));
					listeMessages.remove(0);
				}
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			
		}
	}
	public void envoiMessage(String message){
		for (SessionUtilisateur s : listeUtilisateurs) {
			try {
				s.envoyerMessage(message);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
}
