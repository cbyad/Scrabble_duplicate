package com.pc2r.serveur;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;

public class SessionUtilisateur extends Thread{
	protected Socket client ;
	protected Scrabble scrab;
	boolean quitte = false;
	protected OperationsSurScrabble opScrab;
	public SessionUtilisateur(Socket clientC, Scrabble scrabC, OperationsSurScrabble opScrabC) {
		scrab = scrabC;
		client = clientC;
		opScrab = opScrabC;
	}

	public void run(){
		try{

			String ligne;
			String reponse;
			Scanner in = new Scanner(client.getInputStream());
			while(!quitte){
				ligne = in.nextLine();
				reponse = traitementCommande(ligne);
				
				synchronized (scrab.joueurs) {
					scrab.joueurs.notify();
				}
				envoyerMessage(reponse);
			}
			client.close();
			in.close();
		}

		catch(IOException e){
			System.out.println(e.getMessage());
		}

	}

	public synchronized String traitementCommande(String ligne) throws IOException {
		String[] parse = ligne.split("/");
		switch (parse[0]) {
		case "CONNEXION":
			return opScrab.connexion(this,parse[1]);
		case "SORT":
			quitte = true;
			opScrab.deconnexion(this,parse[1]);
			return null;
		case "TROUVE":
			String res = opScrab.valide(this,parse[1]);
			if(res=="OK"&&scrab.phase=="REC")
				return "RVALIDE/";
			else if(res == "OK"&&scrab.phase == "SOU")
				return "SVALIDE/";
			else if(scrab.phase=="REC")
				return "RINVALIDE/"+res+"/";
			else{
				System.out.println("état: "+scrab.phase);
				return "SINVALIDE/"+res+"/";
			}
		case "ENVOI":
			opScrab.envoiMsg(parse[1]);
			return null;
		case "PENVOI":
			opScrab.envoiPmsg(this,parse[1],parse[2]);
			return null;
		case "MEILLEUR":
			return opScrab.estMeilleur(this);
		default:
			return null ;
		}
		//return ligne;
	}
	public void envoyerMessage(String message) throws IOException{		
		if(message==null)
			return;
		PrintWriter out = new PrintWriter(client.getOutputStream(), true);
		out.println(message);
	}
}
