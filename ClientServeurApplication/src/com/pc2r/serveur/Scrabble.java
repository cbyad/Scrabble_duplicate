package com.pc2r.serveur;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Scrabble extends Thread{
	protected int nbrJoueurs;
	protected HashMap<String, Integer> scoreLisible;
	protected char[][] placementLisible = new char[15][15];
	protected HashMap<String, SessionUtilisateur> joueurs;
	protected HashMap<SessionUtilisateur, String> joueursInverse;
	protected int idJoueur;
	protected ArrayList<String> lettres;
	protected ArrayList<String> lettresCourantes;
	protected int meilleuScoreCourant;
	protected boolean estValide;
	protected String placement;
	protected HashMap<String, Integer> scoreCourrant;
	protected String tirage;
	protected String scores; // hashMap pour le traitement
	protected String phase;
	protected int temps; //temps restant dans la phase
	protected HashMap<String, Integer> valeurLettres;
	protected char[][]placementMeilleur;
	protected int nbSoumission;
	protected String meilleurMot; // A gerer dans un second temps
	protected String leaderCourant;
	protected int nbTour;
	protected ArrayList<String> nomJoueurs;
	protected String motPose;
	protected boolean valide;
	protected Messager messager;
	protected String meilleurJoueurCourant;
	protected ArrayList<String> lettresUtilise;

	public Scrabble(Messager messagerC) {

		StringBuilder str = new StringBuilder() ;

		for (int i = 0; i < 225; i++) {
			str.append(0);
		}
		joueurs = new HashMap<>();
		joueursInverse = new HashMap<>();
		idJoueur = 0;
		estValide = false;
		placement= str.toString() ;
		tirage = "videx";
		scores = "";
		phase = "DEB";
		temps = 20;
		nbrJoueurs = 0;
		nomJoueurs = new ArrayList<>();
		initScoreLisible();
		scores ="0";
		// a revoir  
		meilleurJoueurCourant = new String();
		messager = messagerC;
		lettres = new ArrayList<>();
		lettres = initLettres();
		valeurLettres = new HashMap<>();
		initValeur(valeurLettres);
		placementLisible = stringVersTab(placement);
		placementMeilleur = placementLisible.clone();
		lettresUtilise = new ArrayList<>();
		lettresCourantes = new ArrayList<>();
		messager.start();

	}

	//initialise la variable scoreLisible
	private void initScoreLisible() {
		scoreLisible = new HashMap<>();
		for (String s : nomJoueurs) {
			scoreLisible.put(s, 0);
		}

	}
	
	//initialise les valeur associé à une lettre
	private void initValeur(HashMap<String, Integer> valeurLettres){
		valeurLettres.put("A",1);
		valeurLettres.put("B",3);
		valeurLettres.put("C",3);
		valeurLettres.put("D",2);
		valeurLettres.put("E",1);
		valeurLettres.put("F",4);
		valeurLettres.put("G",2);
		valeurLettres.put("H",4);
		valeurLettres.put("I",1);
		valeurLettres.put("J",8);
		valeurLettres.put("K",10);
		valeurLettres.put("L",1);
		valeurLettres.put("M",2);
		valeurLettres.put("N",1);
		valeurLettres.put("O",1);
		valeurLettres.put("P",3);
		valeurLettres.put("Q",8);
		valeurLettres.put("R",1);
		valeurLettres.put("S",1);
		valeurLettres.put("T",1);
		valeurLettres.put("U",1);
		valeurLettres.put("V",4);
		valeurLettres.put("W",10);
		valeurLettres.put("X",10);
		valeurLettres.put("Y",10);
		valeurLettres.put("Z",10);
		valeurLettres.put("*",0);
	}

	//initalise le pot de lettre selon les proportions classiques
	public ArrayList<String> initLettres(){
		ArrayList<String> list = new ArrayList<>();
		int nbA = 9;
		int nbBCFGHPV = 2;
		int nbDM = 3;
		int nbE = 15;
		int nbI = 8;
		int nbJKQWXYZ = 1;
		int nbL = 5;
		int nbNORSTU = 6;


		for (int i = 0; i < 15; i++) {
			if(i < nbA)
				list.add("A");
			if(i < nbBCFGHPV){
				list.add("B");
				list.add("C");
				list.add("F");
				list.add("G");
				list.add("H");
				list.add("P");
				list.add("V");
				list.add("*");
			}
			if(i < nbDM){
				list.add("D");
				list.add("M");
			}
			if(i < nbE)
				list.add("E");

			if(i < nbI)
				list.add("I");

			if(i < nbJKQWXYZ){
				list.add("J");
				list.add("K");
				list.add("Q");
				list.add("W");
				list.add("X");
				list.add("Y");
				list.add("Z");
			}
			if(i < nbL)
				list.add("L");

			if(i < nbNORSTU){
				list.add("N");
				list.add("O");
				list.add("R");
				list.add("S");
				list.add("T");
				list.add("U");
			}

		}
		return list;
	}
	//réalisation d'une partie
	public void run(){
		while(true){
			synchronized (this.joueurs) {
				try {
					this.joueurs.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			messager.listeMessages.add("SESSION/");
			lettres = initLettres();
			while(!lettres.isEmpty()){
				synchronized (this) {
					lettresCourantes = tireSeptLettres(lettresCourantes, lettresUtilise);		//effectuer le tirage ok
					temps = 20;
				}
				try {
					Thread.sleep(1000); // 20 secondes ici
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchronized (this) {
					tirage = arrayListVersString(lettresCourantes);
				}
				String message = "TOUR/"+placement+"/"+tirage+"/";
				phase = "REC";		//dit au broadcaster de diffuser l'état du jeu + tirage
				synchronized (messager) {
					messager.listeMessages.add(message);
					messager.notify();	
				}
				synchronized (this) {
					try {
						phase = "REC";
						temps = 300;
						this.wait(300000);// 5 mins de recherche
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				//generer un message pour dire que c'est le début de la phase de soummision
				synchronized (this) {
					try {
						phase  = "SOU";
						temps = 120;
						this.wait(120000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				scores = scoreLisibleVersString(scoreLisible); // On a fini la phase de soumission
				if(phase == "SOU"){
					synchronized (this) {
						phase = "RES";
						nbSoumission = 0;
						placementLisible = placementMeilleur.clone();
					}
					messager.listeMessages.add("BILAN/"+meilleurMot+"/"+leaderCourant+"/"+scores+"/");
					synchronized (messager) {
						messager.notify();
					}
				}
				else if (phase == "REC"){ // cas où l'on a attenteint la limite de temps
					synchronized (this) {
						temps = 0;
						phase = "RES";
						lettres.addAll(lettresCourantes);
						nbSoumission = 0;
					}
					messager.listeMessages.add("SFIN/");
					messager.listeMessages.add("BILAN/"+meilleurMot+"/"+leaderCourant+"/"+scores+"/");
					synchronized (messager) {
						messager.notify();

					}
				}
				synchronized (this) {
					placement = placementLisibleVersString(placementLisible);
				}
			}
			messager.listeMessages.add("VAINQUEUR/"+scores+"/");
			synchronized (messager) {
				messager.notify();
			}
		}
	}
	//retourne une liste de 7 lettres( en prenant en compte celle qui n'ont pas été utilisé lors du tour précédent)
	public ArrayList<String> tireSeptLettres(ArrayList<String> lettresCourante,ArrayList<String> lettresUtilise){
		int i = 0;
		if(lettresUtilise.size()!=0)
			i = 7- lettresUtilise.size();
		Random rand = new Random();
		ArrayList<String> res = (ArrayList<String>) lettresCourante.clone();
		for (String s : lettresUtilise) {
			res.remove(res.indexOf(s));
		}
		int index;
		while(i<7||lettres.isEmpty()){
			index = rand.nextInt(lettres.size());
			res.add(lettres.get(index));
			lettres.remove(index);
			i++;
		}
		return res;
	}
	
	
	//crée un plateau à partir d'une chaine de caractère
	public char[][] stringVersTab(String chaine){//Validée
		int k = 0;
		char[][] placementTempon = new char[15][15];
		for (int j = 0; j < 15; j++) {
			for (int i = 0; i < 15; i++) {
				placementTempon[i][j] = chaine.charAt(k);
				k++;
			}
		}
		return placementTempon;
	}
	
	//Transforme une hashmap de score en la chaine caractère possible à retourner
	public String scoreLisibleVersString(HashMap<String, Integer> scoreLisible2) { 		
		StringBuilder s = new StringBuilder(); 		
		s.append(nbrJoueurs+"*"); 		
		for (String n : nomJoueurs) { 			
			s.append(n+"*"); 			
			s.append(scoreLisible2.get(n)+"*"); 		
		} 		
		return s.toString(); 	
	}
	
	//transforme un tableau de placement en une chaine de caractère possible à retourner
	public String placementLisibleVersString(char[][] placementTempon) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < placementTempon.length; i++) {
			for (int j = 0; j < placementTempon.length; j++) {
				s.append(placementTempon[j][i]);

			}

		}
		return s.toString();
	}
	
	//transforme la liste de lettre en liste de lettre(collé)
	public String arrayListVersString(ArrayList<String> lettres){
		StringBuilder s = new StringBuilder();
		for (String l : lettres) {
			s.append(l);
		}
		return s.toString();
	}

}
