package com.pc2r.serveur;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

//Fonction permettant de modifier le scrabble
public class OperationsSurScrabble {
	private Scrabble scrab;
	public OperationsSurScrabble(Scrabble scrabC) {
		scrab = scrabC;

	}
	
	//------------fonction appelé par une SessionUtilisateurs------------//
	
	//permet à un utilisateur de se connnecter si possible
	public String connexion(SessionUtilisateur s,String nom){
		if(!scrab.joueurs.containsKey(nom)){
			scrab.nbrJoueurs ++;
			scrab.joueurs.put(nom, s);
			scrab.joueursInverse.put(s,nom);
			scrab.nomJoueurs.add(nom);
			scrab.scoreLisible.put(nom, 0);
			scrab.messager.listeMessages.add("CONNECT/"+nom+"/");
			synchronized (scrab.messager) {
				scrab.messager.notify();
			}
			scrab.messager.listeUtilisateurs.add(s);
			return "BIENVENUE/"+scrab.placement+"/"+scrab.tirage+"/"+scrab.scores+"/"+scrab.phase+"/"+scrab.temps+"/"; //score à init
		}
		else{
			return "REFUS/";
		}

	}
	
	//permet à un utilisateur de se deconnecter
	public void deconnexion(SessionUtilisateur s,String nom){
		scrab.joueurs.remove(nom);
		scrab.joueursInverse.remove(s);

		scrab.nbrJoueurs--;
		String reponse = "DECONNEXION/"+nom+"/";

		scrab.messager.listeUtilisateurs.remove(s);
		synchronized (scrab.messager) {
			scrab.messager.listeMessages.add(reponse);
			scrab.messager.notify();
		}
	}

	// le mot proposé par est-il valide 
	public synchronized String valide(SessionUtilisateur s,String string) {
		scrab.placementMeilleur = stringVersTab(string);
		scrab.placementLisible = stringVersTab(scrab.placement);
		ArrayList<lettre> nouvelleLettre = trouveLettresAjouté(scrab.placementMeilleur, scrab.placementLisible);
		int score[] = scoreEtValide(nouvelleLettre);
		if(score[0] == -1){
			scrab.estValide = false;
			return "POS";
		}

		scrab.estValide = true;
		scrab.messager.listeMessages.add("RATROUVE/"+scrab.joueursInverse.get(s)+"/");
		synchronized(scrab.messager){
			scrab.messager.notify();
		}
		if(scrab.phase == "REC"){
			changePhase(s,scrab.placementMeilleur,score[1], nouvelleLettre);
		}
		else if(scrab.phase == "SOU"){
			ajouteNouveauMot(s, scrab.placementMeilleur, score[1], nouvelleLettre);
		}
		synchronized (this) {
			this.notify();
		}
		return "OK";
	}

	//sessionUtilisateur est il le meilleur de la manche?
	public String estMeilleur(SessionUtilisateur sessionUtilisateur) {
		if(scrab.meilleurJoueurCourant == scrab.joueursInverse.get(sessionUtilisateur))
			return "1";
		return "0";
	}
	
	//envoit un message general
	public void envoiMsg(String msg) {
		//Ajouter à la liste des messages du chat + reveiller le broadcaster car nouveau message
		String reponse = "RECEPTION/"+msg;

		synchronized (scrab.messager) {
			scrab.messager.listeMessages.add(reponse);
			scrab.messager.notify();
		}
	}
	
	//envoit un message à l'utilisateur nom
	public void envoiPmsg(SessionUtilisateur s,String nom, String msg) throws IOException {
		SessionUtilisateur cible = scrab.joueurs.get(nom);
		String reponse = "PRECEPTION/"+msg+"/"+nom+"/";
		cible.envoyerMessage(reponse);
	}

	
//-------------------A partir de là  fonction utilisé par les fonctions principales----------------//
	private void ajouteNouveauMot(SessionUtilisateur s, char[][] placementTempon, int score, ArrayList<lettre> lettresUtilise2){
		if(score > scrab.meilleuScoreCourant){
			scrab.meilleuScoreCourant = score;
			scrab.leaderCourant = scrab.joueursInverse.get(s);
			scrab.meilleurJoueurCourant = scrab.joueursInverse.get(s);
			scrab.meilleurMot = scrab.motPose;
			for(int i=0;i<lettresUtilise2.size();i++){
				scrab.lettresUtilise.add(lettresUtilise2.get(i).getCaractere()+"");
			}
		}
		int scoreTemp = scrab.scoreLisible.get(scrab.joueursInverse.get(s));
		scrab.scoreLisible.put(scrab.joueursInverse.get(s), scoreTemp+score);
		scrab.nbSoumission++;
	}
	
	//retourne le score du mot entré
	private int calculScoreMot(String mot) {
		int score = 0;
		if(mot.length() == 0)
			return 0;
		for(int i = 0; i<mot.length() ; i++){
			score += scrab.valeurLettres.get(mot.substring(i,i+1));
		}
		return score;
	}
	
	private int[] calculDonneesX(ArrayList<lettre> nouvelleLettre, int i, int[] tabX){

		if(nouvelleLettre.get(i).getY() == tabX[2] || tabX[2] == -1){
			if(tabX[0] > nouvelleLettre.get(i).getX())
				tabX[0] = nouvelleLettre.get(i).getX();
			if(tabX[1] < nouvelleLettre.get(i).getX())
				tabX[1] = nouvelleLettre.get(i).getX();
			tabX[2] = nouvelleLettre.get(i).getY();
			tabX[3]++;
		}
		return tabX;
	}

	private int[] calculDonneesY(ArrayList<lettre> nouvelleLettre, int i, int[] tabX){

		if(nouvelleLettre.get(i).getX() == tabX[2] || tabX[2] == -1){
			if(tabX[0] > nouvelleLettre.get(i).getY())
				tabX[0] = nouvelleLettre.get(i).getY();
			if(tabX[1] < nouvelleLettre.get(i).getY())
				tabX[1] = nouvelleLettre.get(i).getY();
			tabX[2] = nouvelleLettre.get(i).getX();
			tabX[3]++;
		}
		return tabX;
	}


	//Verifie la validité le plateau est vide
	private int[] verificationCasVide(ArrayList<lettre> nouvelleLettre, int[] tabX, int[] tabY) {
		int[] tab = new int[2];
		scrab.valide = false;
		for (lettre l : nouvelleLettre) {
			if(l.getX()==7&& l.getY()==7)
				scrab.valide = true;
		}
		if((!scrab.valide) || ( (tabX[1]-tabX[0]+1!=nouvelleLettre.size())&&tabY[1]-tabY[0]+1!=nouvelleLettre.size() ) ){ // pas au centre ou si il y a des troues

			tab[0] = -1;
			tab[1] = 0;
			return tab;
		}
		tab[0] = 1;
		tab[1] = calculScoreMot(arrayLettreVersString(nouvelleLettre));
		return tab;
	}

	private int sens(ArrayList<lettre> nouvelleLettre){
		int nbX = 0;
		int nbY = 0;
		int tempX = -1;
		int tempY = -1;
		for (lettre l : nouvelleLettre) {
			if(l.getX() == tempX||tempX == -1){
				tempX = l.getX();
				nbX++;
			}
			if(l.getY() == tempY||tempY == -1){
				tempY = l.getY();
				nbY++;
			}
		}
		if(nbX == nouvelleLettre.size()){
			return 1; //vertical
		}
		if(nbY == nouvelleLettre.size()){
			return 0; //horizontal
		}
		return -1;

	}

	private int pointAutreMotsHoriz(ArrayList<lettre> nouvelleLettre, int[] minMax) {
		scrab.valide = false;
		boolean entree = false;
		int i = 1;
		int scoreTemp = 0;
		int y = 0;
		StringBuilder motTemp = new StringBuilder();
		//vérifier si le mot touche une lettre précédente

		for (lettre l : nouvelleLettre) {
			y = l.getY();
			motTemp = new StringBuilder();

			if(l.getY()>0 && scrab.placementLisible[l.getX()][l.getY()-1]!='0'){//prolongement d'un mot venant du haut
				scrab.valide = true;
				entree = true;
				motTemp.append(l.caractere);
				i = 1;
				while(l.getY()-i>0&&scrab.placementLisible[l.getX()][l.getY()-i] != '0'){
					motTemp.append(scrab.placementLisible[l.getX()][l.getY()-i]);
					i++;
				}
			}
			if(l.getY()<14 && scrab.placementLisible[l.getX()][l.getY()-1]!='0'){//prolongement d'un mot venant du bas
				scrab.valide = true;
				if(!entree)
					motTemp.append(l.caractere);
				i = 1;
				while(l.getY()+i<140&&scrab.placementLisible[l.getX()][l.getY()+i] != '0'){
					motTemp.append(scrab.placementLisible[l.getX()][l.getY()+i]);
					i++;

				}
			}
		}
		//prolongement d'un mot horizontalement
		scoreTemp += calculScoreMot(motTemp.toString());
		i = minMax[0]-1;
		motTemp = new StringBuilder();
		if(nouvelleLettre.size() == 1+minMax[1]-minMax[0]){ // on a pas de croisement
			for (lettre l2 : nouvelleLettre) {
				motTemp.append(l2.caractere);
			}
		}
		Boolean fin = false;
		while(i>=0&&!fin){
			if(scrab.placementLisible[i][y]!='0')
				motTemp.append(scrab.placementLisible[i][y]);
			else
				fin = true;
			i--;
		}
		i = minMax[1]+1;
		fin = false;
		while(i<15&&!fin){
			if(scrab.placementLisible[i][y]!='0')
				motTemp.append(scrab.placementLisible[i][y]);
			else
				fin = true;
			i++;
		}
		scoreTemp += calculScoreMot(motTemp.toString());

		return scoreTemp;
	}
	private String calculMotCroisementHoriz(ArrayList<lettre> nouvelleLettre, int[] minMaxHori) {
		StringBuilder mot = new StringBuilder();
		int y = nouvelleLettre.get(0).getY();
		for (int i = minMaxHori[0]; i <= minMaxHori[1]; i++) {
			if(scrab.placementLisible[i][y]!='0'){
				mot.append(scrab.placementLisible[i][y]);
			}
			else{
				for (lettre l : nouvelleLettre) {
					if(l.getX() == i){
						mot.append(l.getCaractere());
						break;
					}
				}
			}
		}
		if(1+minMaxHori[1]-minMaxHori[0] == mot.toString().length() )
			scrab.valide = true;
		return mot.toString();
	}

	private int[] minMaxHorizontal(ArrayList<lettre> nouvelleLettre) {
		int[] minMaxX = new int[2];
		minMaxX[0] = Integer.MAX_VALUE;//min
		minMaxX[1] = 0;//max
		for (lettre l : nouvelleLettre) {
			if(minMaxX[0] > l.getX()){
				minMaxX[0] =l.getX();
			}
			if(minMaxX[1] < l.getX()){
				minMaxX[1] = l.getX();
			}

		}
		return minMaxX;
	}


	private int[] minMaxVertical(ArrayList<lettre> nouvelleLettre) {
		int[] minMaxY = new int[2];
		minMaxY[0] = Integer.MAX_VALUE;//min
		minMaxY[1] = 0;//max
		for (lettre l : nouvelleLettre) {
			if(minMaxY[0] > l.getY()){
				minMaxY[0] =l.getY();
			}
			if(minMaxY[1] < l.getY()){
				minMaxY[1] = l.getY();
			}

		}
		return minMaxY;
	}

	private ArrayList<lettre> trouveLettresAjouté(char[][] placementMeilleur2, char[][] placementLisible2) {//A priori le meilleur est ok le lisible décalé
		ArrayList<lettre> nouvelleLettres= new ArrayList<>();
		for (int i = 0; i < placementLisible2.length; i++) {
			for (int j = 0; j < placementLisible2.length; j++) {
				if(placementLisible2[j][i] != placementMeilleur2[j][i]){
					nouvelleLettres.add(new lettre(placementMeilleur2[j][i],j,i));
				}
			}

		}
		return nouvelleLettres;
	}
	private String arrayLettreVersString(ArrayList<lettre> nouvelleLettres){
		StringBuilder str = new StringBuilder();
		for (lettre l : nouvelleLettres) {
			str.append(l.caractere);
		}
		return str.toString();
	}


	public boolean estVide(String annexe){
		int var = 0;
		for (int i = 0; i < annexe.length(); i++) {
			if(annexe.charAt(i)=='0')
				var++;
		}
		if(var == annexe.length())
			return true;
		return false;	
	}
	
	private int pointAutreMotsVert(ArrayList<lettre> nouvelleLettre, int[] minMax) { 
		scrab.valide = false;
		boolean entree = false;
		int i = 1;
		int scoreTemp = 0;
		int x = 0;
		StringBuilder motTemp = new StringBuilder();
		//vérifier si le mot touche une lettre précédente

		for (lettre l : nouvelleLettre) {
			x = l.getX();
			motTemp = new StringBuilder();
			if(l.getX()>0 && scrab.placementLisible[l.getX()-1][l.getY()]!='0'){//prolongement d'un mot venant de la gauche
				scrab.valide = true;
				entree = true;
				motTemp.append(l.caractere);
				i = 1;
				while(l.getX()-i>=0&&scrab.placementLisible[l.getX()-i][l.getY()] != '0'){
					motTemp.append(scrab.placementLisible[l.getX()-i][l.getY()]);
					i++;
				}
			}
			if(l.getX()<14 && scrab.placementLisible[l.getX()-1][l.getY()]!='0'){//prolongement d'un mot venant de la droite
				scrab.valide = true;
				if(!entree)
					motTemp.append(l.caractere);
				i = 1;
				while(l.getX()+i<=14&&scrab.placementLisible[l.getX()+i][l.getY()] != '0'){
					///
					motTemp.append(scrab.placementLisible[l.getX()+i][l.getY()]);
					i++;

				}
			}
		}
		//prolongement d'un mot vertical
		scoreTemp += calculScoreMot(motTemp.toString());
		i = minMax[0]-1; // minimum de mon mot
		motTemp = new StringBuilder();
		if(nouvelleLettre.size() == 1+minMax[1]-minMax[0]){ // on a pas de croisement
			for (lettre l2 : nouvelleLettre) {
				motTemp.append(l2.caractere);
			}
		}
		Boolean fin = false;
		while(i>=0&&!fin){
			if(scrab.placementLisible[x][i]!='0')
				motTemp.append(scrab.placementLisible[x][i]);
			else
				fin = true;
			i--;
		}
		i = minMax[1]+1;
		fin = false;
		while(i<15&&!fin){
			if(scrab.placementLisible[x][i]!='0')
				motTemp.append(scrab.placementLisible[x][i]);
			else
				fin = true;
			i++;
		}
		scoreTemp += calculScoreMot(motTemp.toString());

		return scoreTemp;
	}

	//trouve le sens et les trous, vérifie la validité de la position des lettres
	private int[] scoreEtValide(ArrayList<lettre> nouvelleLettre) {
		int scoreTemp = 0;
		int[] tab = new int[2]; // tableau de retour
		int[] tabX = new int[4];// minX, maxX, valX, varX
		tabX[0] = Integer.MAX_VALUE;
		tabX[1] = -1;
		tabX[2] = -1;
		tabX[3] = 0;

		int[] tabY = new int[4];// minY, maxY, valY, varY
		tabY[0] = Integer.MAX_VALUE;
		tabY[1] = -1;
		tabY[2] = -1;
		tabY[3] = 0;
		for (int i = 0; i < nouvelleLettre.size(); i++) {
			tabX = calculDonneesX(nouvelleLettre, i, tabX);
			tabY = calculDonneesY(nouvelleLettre, i, tabY);
		}

		if(estVide(scrab.placement)){ // Premier placement de lettre
			return verificationCasVide(nouvelleLettre, tabX, tabY);
		}
		int sens = sens(nouvelleLettre);
		if(sens == -1){
			tab[0] = -1;
			tab[1] = 0;
			return tab;
		}
		if(sens == 0){ //Cas d'un mot horizontal
			int[] minMaxHori;
			minMaxHori = minMaxHorizontal(nouvelleLettre);
			if(1+minMaxHori[1]-minMaxHori[0] == nouvelleLettre.size()){ // mot sans croisement
				//On doit tester si ça touche un autre mot
				scoreTemp += pointAutreMotsHoriz(nouvelleLettre, minMaxHori);
				scrab.motPose = arrayListVersString2(nouvelleLettre);
				if(!scrab.valide){
					tab[0] = -1;
					tab[1] = 0;
					return tab;
				}
				tab[0] = 0;
				tab[1] = scoreTemp + calculScoreMot(scrab.motPose);
				return tab;
			}
			else{//il y a donc un croisement
				boolean validation = false;
				scoreTemp += pointAutreMotsHoriz(nouvelleLettre, minMaxHori);
				if(scrab.valide){
					validation = true;
				}
				scrab.motPose = calculMotCroisementHoriz(nouvelleLettre, minMaxHori);
				if(!scrab.valide&&!validation){
					tab[0] = -1; // Il y a un erreur de score en sortie attention
					tab[1] = 0;
					return tab;
				}
				tab[0] = 1;
				tab[1] = scoreTemp + calculScoreMot(scrab.motPose);
				return tab;
			}

		}
		else 
		{ //Cas d'un mot vertical
			int[] minMaxVert;
			minMaxVert = minMaxVertical(nouvelleLettre);
			if(1+minMaxVert[1]-minMaxVert[0] == nouvelleLettre.size()){//mot vertical sans croisement
				scoreTemp += pointAutreMotsVert(nouvelleLettre, minMaxVert);
				String motPose = arrayListVersString2(nouvelleLettre);
				if(!scrab.valide){
					tab[0] = -1;
					tab[1] = 0;
					return tab;
				}
				tab[0] = 0;
				tab[1] = scoreTemp + calculScoreMot(motPose);
				return tab;
			}
			else{//il y a un croisement
				boolean validation = false;
				scoreTemp += pointAutreMotsVert(nouvelleLettre, minMaxVert);
				if(scrab.valide)
					validation = true;
				scrab.motPose = calculMotCroisementVert(nouvelleLettre, minMaxVert);
				if(!scrab.valide&&!validation){
					tab[0] = -1;
					tab[1] = 0;
					return tab;
				}
				tab[0] = 1;
				tab[1] = scoreTemp + calculScoreMot(scrab.motPose);
				return tab;
			}
		}
	}

	public  String arrayListVersString2(ArrayList<lettre> nouvelleLettre) {
		StringBuilder mot = new StringBuilder();
		for (lettre l : nouvelleLettre) {
			mot.append(l.getCaractere());
		}
		return mot.toString();
	}
	
	public String calculMotCroisementVert(ArrayList<lettre> nouvelleLettre, int[] minMaxVert) {
		StringBuilder mot = new StringBuilder();
		int x = nouvelleLettre.get(0).getX();
		for (int i = minMaxVert[0]; i < minMaxVert[1]; i++) {
			if(scrab.placementLisible[x][i]!='0'){
				mot.append(scrab.placementLisible[x][i]);
			}
			else{
				for (lettre l : nouvelleLettre) {
					if(l.getY() == i){
						mot.append(l.getCaractere());
						break;
					}
				}
			}
		}
		return mot.toString();
	}
	
	private void changePhase(SessionUtilisateur s,char[][] placementTempon, int score, ArrayList<lettre> lettreUtilise2) {
		scrab.scoreCourrant = new HashMap<>();
		scrab.scoreCourrant.put(scrab.joueursInverse.get(s), score);
		scrab.phase = "SOU";
		scrab.leaderCourant = scrab.joueursInverse.get(s);
		scrab.meilleuScoreCourant = score;
		scrab.placementMeilleur = placementTempon.clone();
		int scoreTemp = scrab.scoreLisible.get(scrab.joueursInverse.get(s));
		scrab.scoreLisible.put(scrab.joueursInverse.get(s), scoreTemp+score);
		scrab.nbSoumission = 1;
		scrab.meilleurMot = scrab.motPose;
		char temp;
		for(int i=0;i<lettreUtilise2.size();i++){
			temp = lettreUtilise2.get(i).getCaractere();
			scrab.lettresUtilise.add(temp+"");
		}
	}

	
	
	
	public char[][] stringVersTab(String chaine){//Validée
		int k = 0;
		char[][] placementTempon = new char[15][15];
		for (int j = 0; j < 15; j++) {
			for (int i = 0; i < 15; i++) {
				placementTempon[i][j] = chaine.charAt(k);
				k++;
			}
		}
		return placementTempon;
	}
}
